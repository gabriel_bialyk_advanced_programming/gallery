<table align="center"><tr><td align="center" width="9999">
<img src="https://www.pngitem.com/pimgs/m/80-801467_gallery-white-gallery-icon-png-transparent-png.png" align="center" width="150" alt="Project icon">

# Gallery

Gallery project by Gabriel Bialyk
</td></tr></table>


## Table of contents

- [Gallery](#Gallery)
	- [Table of contents](#table-of-contents)
	- [Quick start](#quick-start)
	- [Status](#status)
	- [What's included](#whats-included)
	- [Creator](#creator)

## Quick start

- Open The releases tab and download the GalleryConsole.exe file - (Console app)
- Open The releases tab and download the GallerySetup.exe & serverFolder (run server.exe) - (Gui app & server)

## Status

- Finish V2 - Gallery GUI & RestAPI.

## What's included

```text
.CPP, .HPP, .Sln (plus extra for vs), 
.CS, .Js (for the server), (releases)
```

## Creator

**G.B**

- <https://gitlab.com/Toxey>